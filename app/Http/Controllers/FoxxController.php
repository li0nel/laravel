<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FoxxController extends Controller
{
    public function helloworld() {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://internal-orchardai-default-alb-1348422120.eu-west-2.elb.amazonaws.com:8529/_db/_system/api/foxx-hello-world');
       
        return json_decode($request->getBody()->getContents(), true);
    }
}
